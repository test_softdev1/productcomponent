/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.mycompanent;

import java.util.ArrayList;

/**
 *
 * @author Lenovo
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"espresso 1",30,"1.jpg"));
        list.add(new Product(2,"espresso 2",40,"2.jpg"));
        list.add(new Product(3,"espresso 3",50,"3.jpg"));
        list.add(new Product(4,"americano 1",30,"1.jpg"));
        list.add(new Product(5,"americano 2",40,"2.jpg"));
        list.add(new Product(6,"americano 3",50,"3.jpg"));
        list.add(new Product(7,"chayen 1",30,"1.jpg"));
        list.add(new Product(8,"chayen 2",40,"2.jpg"));
        list.add(new Product(9,"chayen 3",50,"3.jpg"));
        return list;
    }
}
